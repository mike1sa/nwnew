---
weight: 1
bookFlatSection: true
title: "About Us"
---

# Introduction

## Our Story
For the past 15 years Mike has been working as a mechanic, starting out at his uncle’s motorcycle shop then moving on to diesel engines, eventually making his way into the marine industry for 10 years working on everything from commercial fishing boats to some of the biggest luxury yachts ever made from all over the world. Mike quickly became the go to guy for the obscure and uncommon boat builds that needed specialized hydraulic, electrical work, repowers, overhauls, custom fabrication, machining and elaborate rigging. Since coming to North Idaho in 2021 Mike has delved into heavy equipment work eventually starting his own mobile mechanic business Victory Repair which covers heavy equipment, boats and welding, Mike wanted to fill a void he has seen in the line of mobile mechanics especially for uncommon or ‘grey market’ machines dealers will not touch. Mike prides himself on solving problems and helping people and small businesses out in an emergency, taking the burden off the customer and handling everything.

In order to launch his next venture into line boring, Mike needed capitol and some regional experience so Victory Repair was designed to be the kick starter business for Northwest Line Boring.
